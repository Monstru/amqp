<?php 
require_once __DIR__ . '/../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('localhost', 5672, 'admin', 'admin');
$channel = $connection->channel();

//Permanent loop to get messages from the queue
get_messages($connection,$channel);


function get_messages($connection, $channel){
    // Create a connection
    $channel->queue_declare('order', false, false, false, false);

    echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

    $callback = function ($msg) {
        //Update the status of the command
        echo ' [x] Received ', $msg->body, "\n";
        //Get id field in json
        $json = json_decode($msg->body, true);
        $id = $json['id'];
        update_order($id);
        echo " [x] Status de la commande $id modifié\n";
        //Send message to exchange
        send_message_exchange();

    };
    $channel->basic_consume('order', '', false, true, false, false, $callback);

    while ($channel->is_open()) {
        $channel->wait();
    }

    $channel->close();
    $connection->close();
}
//Update order status when the message is received
function update_order($id){


    $status = "Commande traitée";
    include('../models/db.php');
    $result = mysqli_query(
    $con,
    "UPDATE `commandes` SET `status`='$status' WHERE id=$id");
}

function send_message_exchange(){
    $connection = new AMQPStreamConnection('localhost', 5672, 'admin', 'admin');
    $channel = $connection->channel();
    
    $channel->exchange_declare(
        'order_validated',  # name
        'fanout', # type
        false,    # passive
        false,    # durable
        false     # auto_delete
    );
    $data = "Plat créé !";  
    $msg = new AMQPMessage($data);
    $channel->basic_publish($msg, 'order_validated');
    echo "Published: $data" . PHP_EOL;
}

?>