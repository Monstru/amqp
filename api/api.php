<?php 
//Check if id is set in the url
if (isset($_GET['id']) && $_GET['id']!="") {
	include('../models/db.php');
	$id = $_GET['id'];
	$result = mysqli_query(
	$con,
	"SELECT * FROM `commandes` WHERE id=$id");
	if(mysqli_num_rows($result)>0){
	$row = mysqli_fetch_array($result);
	$status = $row['status'];
	response($id, $status);
	mysqli_close($con);
	}else{
		response(NULL, NULL, 200,"No Record Found");
		}
}else{
	response('error', 'error', 400,"Invalid Request");
	}

	//Function to send response to the client in JSON format
function response($id,$status){
	$response['id'] = $id;
	$response['status'] = $status;	
	$json_response = json_encode($response);
	echo $json_response;
}
?>