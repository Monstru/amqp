<?php
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

	include('models/db.php');
	//Create a new command
	$sql = "INSERT INTO `commandes` (`id`, `status`) VALUES (NULL, 'Commande en cours de traitement')";
	$result = mysqli_query($con, $sql);
	if($result){
		$jobArray = array(
			'id' => $con->insert_id,
			'task' => 'sleep',
			'sleep_period' => rand(0, 3)
	);
		send_message_to_rabbitmq(json_encode($jobArray, JSON_UNESCAPED_SLASHES));
		header("Location: index.php");
	}else{
		//error
		print_r('Error: '.mysqli_error($con));
	}


function send_message_to_rabbitmq($order){
	// Create a connection
	$connection = new AMQPStreamConnection('localhost', 5672, 'admin', 'admin');
	$channel = $connection->channel();

	$channel->queue_declare('order', false, false, false, false);

	$msg = new AMQPMessage($order, array('delivery_mode' => 2));
	$channel->basic_publish($msg, '', 'order');

	$channel->close();
	$connection->close();
}
?>