<!-- Index html-->
<?php include 'models/db.php'; ?>
<!DOCTYPE html>
<html>
<head>
<title>Creation de commande</title>
</head>
<body>
  <h1>Création de commandes</h1>
  <form action="order.php" method="POST">
    <input type="submit" name="submit" value="Créer une commande">
  </form>
  <h2>Commandes en cours</h2>
  <p>Voici l'ensemble des commandes en cours</p>
  <div class="commandes">
    <?php  $result = mysqli_query($con, "SELECT * FROM `commandes`");
    if(mysqli_num_rows($result)>0){
      while($row = mysqli_fetch_array($result)){
        $id = $row['id'];
        $status = $row['status'];
        echo "<p><strong>Commande n°$id</strong> | status : $status</p><p style='display:flex; gap:20px;'><a href='api/api.php?id=$id'>Voir le retour GET de l'API pour la commande n°$id</a><a href='controllers/delete_order.php?id=$id'>Supprimer la commande n°$id</a></p>";
      }
    } ?>
  </div>
</body>
</html>