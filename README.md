# Cloud Computing #

## Installation du projet ## 
Pour installer le projet, vous aurez besoin de plusieurs éléments :
     - Composer
     - Un rabbitmq de lancé avec docker
     - Un serveur apache pour executer le PHP (comme Wamp ou Laragon)
     - Un service de base de données mySQL comme PHPmyAdmin

Ensuite, vous retrouverez différentes variables à modifier selon votre configuration :

     - Dans le fichier  db.php , vous pouvez modifier le serveur ainsi que les identifiants de connexions à la base de données avec également le nom de la base.
     - Dans le fichier  order.php  , vous pouvez modifier à la ligne 26, les identifiants de votre rabbitmq (ici admin, admin) 
     - Dans le fichier  worker.php , vous pouvez également modifier les informations de connexions à rabbitmq

## Executer le projet 

Afin de lancer le projet, dans un premier temps, commencez par faire un  "composer install" en ligne de commande à la racine du projet afin d'installer les classes de Rabbitmq pour PHP
Ensuite, il faut importer le fichier  db.sql  dans votre phpmyadmin ou via ligne de commande en utilisant SQL afin d'avoir la table créée.

Executez un docker-compose up pour lancer le rabbitmq

Ensuite, vous pouvez simplement lancer l'index.php dans votre navigateur afin de voir le front de l'application. Sur ce front, vous pouvez créer des commandes, voir l'ensemble des commandes créées avec leur status correspondant, et supprimer des commandes. Vous pouvez également, voir le résultat de l'API en cliquant sur  "Voir le retour GET de l'API..." 

Lorsque vous crééz une commande, elle a le statut en cours de traitement et pour appliquer les changements dans Rabbitmq, il vous suffit, en ligne de commande, de lancer le worker.php avec la commande suivante :   php worker.php. Ainsi, le worker se charge d'écouter en temps réel les messages dans la queue order et s'occupe de modifier le statut des commandes concernés lors de la réception du message. Il se charge également d'envoyer un message au moment où une commande a son statut modifié.

## Crédits ## 
Alban Goupil MDS M2 Cloud Computing